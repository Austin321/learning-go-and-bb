FROM golang:1.19-alpine
WORKDIR /first-build

COPY src/ ./

RUN go mod download

RUN go build -o wiki-app

FROM alpine:latest

WORKDIR /root/

COPY --from=0 /first-build/wiki-app ./

CMD [ "./wiki-app" ]