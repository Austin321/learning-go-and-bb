# Questions
1. Is istio usually installed through the cli tool? Seems kinda weird to me to not have it be declaritive, but I suppose it can be done in a pipeline at least 
    1. Maybe it should be installed with helm?
    1. If I want to install it through flux helm release how can I do that and do it in a timely way. Seems like that install needs to finish 
    before everything else is applied
    1. ^ either that or the helm install just isn't working either way the question remains
    Answer: big bang uses something called the istio operator which is a pod that installs istio for you

1. What is doing the equivalent to the trafeik controller and actually allowing my cluster to be reachable from outside the k3d container in istio. I assume it's the gateway
    ans: k3d by default port forwards 80:80 we can set other ports when we make the cluster
1. What is the best practice around installing flux why is it that I don't see the flux-system folder generally
    ans: Sounds like use flux system, though we use big abng anyway
1. Why is this working on port 80 but not port 9080 istio config
    ans: k3d port forward
1. Is it common to have config things like container port set in helm charts to elimiate duplication 
    1. What about kustomize? In that case it could also add duplication unless you add the kustomize to the base 
1. Why make your own helm chart if it's your teams personal app and you're the only one who's going to use it? 
Multiple enviornments? Eliminating duplication?
    1. Why helm over kustomize?
1. Good ways to determine resources necessary for pods 
    Answer: Hard to do - Performance testing.
1. Sometimes you change the names of resources and then they get leftover or you have to delete them manually. How would flux handle this in a git ops scenario? 
    Answer: Prune command in flux, helm gives you this for free. 