k3d cluster delete
k3d cluster create --api-port 6443  --k3s-arg "--disable=traefik@server:0" 
flux install
istioctl install -y
file_contents=$(<./secrets.txt)
kubectl create secret generic gitlab-creds \
  --from-literal=username=austinabro321 \
  --from-literal=password=$file_contents