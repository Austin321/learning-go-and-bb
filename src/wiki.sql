use recordings;
DROP TABLE IF EXISTS wiki;
CREATE TABLE wiki(
  id         INT AUTO_INCREMENT NOT NULL,
  title      VARCHAR(128) NOT NULL,
  body       VARCHAR(3000) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO wiki
  (title, body)
VALUES
  ('Blue-Train', 'hello world'),
  ('Giant-Steps', 'hell unleashed');

select * from recordings.wiki

select * from mysql.user

CREATE DATABASE greeting;
use greeting;
DROP TABLE IF EXISTS hello_world;
create table hello_world(
    id int auto_increment not NULL,
    w char not NULL,
    primary key (`id`)
);

use greeting;
insert into hello_world (w) values ('w');

select * from greeting.hello_world