package main

import (
    "testing"
)

func TestAdd10ToNum(t *testing.T){
	num := 10
	numPlus10 := add10toNum(num)
	if numPlus10 != 20 {
        t.Fatalf("wanted 20")
    }
}