package main

import (
	"github.com/gin-gonic/gin"
	"database/sql"
    _ "github.com/go-sql-driver/mysql"
	"fmt"
	"os"
)

var user   = os.Getenv("DBUSER")
var passwd = os.Getenv("DBPASSWORD")
var host   = os.Getenv("DBHOST")
var dbname = "greeting"

func add10toNum(num int) (int){
	return num + 10
}

func getId() (int,error) {
	db, err := sql.Open("mysql", user+":"+passwd+"@tcp(" + host + ":3306)/"+ dbname)

	// if there is an error opening the connection, handle it
	if err != nil {

		// simply print the error to the console
		fmt.Println("Err", err.Error())
		// returns nil on error
		return -1,fmt.Errorf("error opening connection")
	}
	defer db.Close()

	results, err := db.Query("SELECT id FROM hello_world")

	if err != nil {

		fmt.Println("Err", err.Error())

		return -1,fmt.Errorf("query failed")

	}

	var id int
	for results.Next() {
		err = results.Scan(&id)
		if err != nil {
			panic(err.Error())
		}

		fmt.Println("product.code :", id)
	}
	return id,nil
}

func main() {
	r := gin.Default()

	r.GET("/hello", func(c *gin.Context) {
		c.String(200, "Hello, World!")
	})


	r.GET("/", func(c *gin.Context) {
		c.String(200, "hell unleashed!")
	})

	r.GET("/hell", func(c *gin.Context) {
		id,err := getId()
		println(id)
		if err != nil{
			c.Abort()	
		}
		c.String(200, fmt.Sprintf("%s%d","hell unleashed with id ",id))
	})

	r.GET("/spin", func(c *gin.Context) {
		id,err := getId()
		println(id)
		if err != nil{
			c.Abort()	
		}
		c.String(200, fmt.Sprintf("%s%d","id +10 is ",add10toNum(id)))
	})	

	r.Run(":7979")
}