k3d cluster create --api-port 6443  --k3s-arg "--disable=traefik@server:0"  --port 9080:80@loadbalancer


# todo
1. Try out eliminating duplication 
1. Go CI all the way through to image deploy in gitlab 
1. CI pipeline for helm 
1. Start using SOPS for mysql password 
    Be able to decrypt sops secrets using kustomize
    have to enable alpha plugins
1. Play around with testing framework
1. Play around with argo
1. database migration 

flux command
```bash
flux create secret git wiki-app-auth \
    --url=https://gitlab.com/Austin321/learning-go-and-bb \
    --private-key-file=/home/austin/.ssh/id_ed25519

flux bootstrap gitlab \
  --ssh-hostname=gitlab.com \
  --owner=Austin321 \
  --repository=learning-go-and-bb \
  --branch=main \
  --path=clusters/dev
```